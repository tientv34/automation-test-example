import locator from '../home-page/elHomePage'

class locatorsHomePage {
    seachTextbox() {
        return cy.get(locator.seachTextbox)
    }

    buttonSearch() {
        return cy.get(locator.buttonSearch)
    }
}

export default locatorsHomePage