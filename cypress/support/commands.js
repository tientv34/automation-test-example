// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('openUrl', (url) => {
    cy.visit(url)
})

// Cypress.Commands.overwrite('get', (locators)=> {
//     if (typeof locators == 'string') {
//         return locators
//     }

//     if(locators.toString().startsWith('/') || locators.toString().startsWith('//') || locators.toString().startsWith('(//')) {
//         return cy.xpath(locators)
//     }

//     else {
//         return cy.get(locators)
//     }
// })

const get = (originalFunction, selector, options) => {
    Cypress.Commands.add('originalGet', originalFunction);
  
    if(typeof selector !== 'string') {
      return selector;
    }
  
    if(selector.startsWith('//') || selector.startsWith('.//') || selector.startsWith('(//') || selector.startsWith('((//')) {
      return cy.xpath(selector, options);
    }
  
    return originalFunction(selector, options);
  };

Cypress.Commands.overwrite('get', get);
