import locatorsHomePage from '../../fixtures/home-page/locator';

const locatorAtHomePage = new locatorsHomePage()
export function searchAtHomePage(item) {
    locatorAtHomePage.seachTextbox().click().type(item)
    locatorAtHomePage.buttonSearch().click()
}