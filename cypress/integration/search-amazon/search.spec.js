import dataHomePage from '../../fixtures/home-page/data.json'
import { searchAtHomePage } from '../../support/page/home.js'

describe('Search at Amazon', ()=>{
    before(() => {
        cy.openUrl(dataHomePage.linkAmazon)
    })
    it('Test', ()=> {
        searchAtHomePage(dataHomePage.dataSearch)
    })
})